'use strict';

chrome.runtime.onInstalled.addListener(function() {
  chrome.storage.sync.set({color: '#3aa757'}, function() {
    console.log("The color is green.");
  });

  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostPrefix: 'scholar.google'},
        })
        ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
});

var isStarEverythingMode = false;

chrome.runtime.onMessage.addListener(notify);
function notify(message, sender, sendResponse) {
	if (message.hasOwnProperty('page')) {
		console.log('We are in page: ' + message.page);
	} else if (message.hasOwnProperty('starEverything')) {
		console.log('starEverything is now ' + message.starEverything);
		isStarEverythingMode = message.starEverything;
	} else if (message.hasOwnProperty('isStarEverything')) {
		console.log('isStarEverything called, current status is ' + isStarEverythingMode);
		sendResponse({'isStarEverything': isStarEverythingMode});
	}
}

