console.log('Injected!');

var elementsInProgress = 0;
var isStarEverything = false;

function onElementStarred(element) {
	elementsInProgress--;
	console.log('Element starred: ' + element + ', ' + elementsInProgress + ' left');
	if (elementsInProgress === 0) {
		console.log('All elements starred');
		onAllElementsStarred();
	}
}

function onAllElementsStarred() {
	if (isStarEverything) {
		console.log('Star everything mode - proceeding to next page');
		let nextButton = document.querySelector('[aria-label="Next"]');
		if (nextButton && !nextButton.disabled) {
			nextButton.click();	
		} else {
			console.log('Next button was not found or disabled, stopping run');
			stopStarEverything();	
		}
		
	}
}

function starAll() {
	console.log('starAll Called');
	let allStarElementsArray = Array.from(document.getElementsByClassName("gs_or_sav"));
	elementsInProgress = allStarElementsArray.length;
	allStarElementsArray.forEach(function(el) {
	    var elementToClick = el;
	    var waitTimeWindow = 5000;
	    var waitTime = Math.floor(Math.random() * Math.floor(waitTimeWindow));
	    setTimeout(function() { 
	    	elementToClick.click();
	    	onElementStarred(elementToClick); 
	    }, waitTime);
	});
}

function starEverything() {
	console.log('starEverything Called');
	chrome.runtime.sendMessage({"starEverything": true });
	isStarEverything = true;
	starAll();
}

function stopStarEverything() {
	console.log('stopStarEverything Called');
	isStarEverything = false;
	chrome.runtime.sendMessage({"starEverything": false });	
}

var navigationPagesList = document.getElementById("gs_nml");
var navigationPagesListElments = navigationPagesList.children;
for (var i = 0; i < navigationPagesListElments.length; i++) {
	let curPageElement = navigationPagesListElments[i];
	if (curPageElement.tagName === 'B') {
		console.log('Found at page ' + i + " is " + curPageElement);
		chrome.runtime.sendMessage({"page": curPageElement.innerHTML });
	}
}

chrome.runtime.sendMessage({'isStarEverything': true}, function(response) {
	console.log('isStarEverything: ' + response.isStarEverything);
	if (response.isStarEverything) {
		starEverything();
	}
});
