let starAllPage = document.getElementById('starAllPage');
let starEverything = document.getElementById('starEverything');


chrome.runtime.sendMessage({'isStarEverything': true}, function(response) {
	let isStarEverything = response.isStarEverything;

	chrome.tabs.executeScript(null, { file: '/injected.js' });

	//starAll.style.backgroundColor = data.color;
	//starAll.setAttribute('value', data.color);
	starAllPage.onclick = function(element) { 
		chrome.tabs.executeScript({ code: 'starAll();'});
	};

	if (!isStarEverything) {
		starEverything.value = 'Star Everything';
		starEverything.onclick = function(element) {
			chrome.tabs.executeScript({ code: 'starEverything();'});
		};
	} else {
		starEverything.value = 'Stop Starring';
		starEverything.onclick = function(element) {
			chrome.tabs.executeScript({ code: 'stopStarEverything();'});
		};
	}

	chrome.storage.sync.get('color', function(data) {
	});

});
